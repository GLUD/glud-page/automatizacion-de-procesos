# Script modular de python para apoyar en la automatización de procesos

Para construir, es recomendable el uso de un entorno virtual

```console

pip install -r requirements.txt

```

Para ejecutar el proyecto

```console

python main.py

```
