#!/bin/python

from abc import ABC, abstractmethod

class Query(ABC):

    @abstractmethod
    def __init__(self, url):
        self.__url = url

    @property
    def url(self):
        return self.__url

    @url.setter
    def url(self, url):
        self.__url = url