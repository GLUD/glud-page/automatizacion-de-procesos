#!/bin/python

from app.interfaces import Query

class Gitlab(Query):

    def __init__(self):
        super().__init__("https://gitlab.com/api/graphql/")